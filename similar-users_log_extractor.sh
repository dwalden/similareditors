#!/bin/bash

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
# http://www.gnu.org/copyleft/gpl.html


# This script pulls statistics from the similar-users logs for each username
# looked up.

# Assuming the similar-users logs have been saved to a file
# ($similar_users_log_file), it will:
# 1. split that file into separate files, one for each username looked up
# 2. derive statistics from each separate file, for example the number of errors
#    reported
# 3. print a csv (or table) with a row for each username and columns for
#    different statistics

similar_users_log_file=""
prefix="xx"
# You probably only want to run this line once, so comment out if you are
# running this script a second time
csplit -f $prefix $similar_users_log_file "/GET \/w\/api.php?action=query&list=usercontribs&ucuser=/" {*}

# echo "| Username | ERROR_count | WARNING_count | CRITICAL_count | results | timeout | keyerror | user nonexistent | line_count | file_name |"
echo "username,ERROR_count,WARNING_count,CRITICAL_count,results,timeout,keyerror,user nonexistent,line_count,file_name"
for file in $prefix*
do
    username=$(grep -o -P "(?<=ucuser=)[^&]*" $file)
    wc=$(wc -l < $file)
    ERROR=$(grep -c ERROR $file)
    WARNING=$(grep -c WARNING $file)
    CRITICAL=$(grep -c CRITICAL $file)
    resultsp=$(grep -c "Returning result of" $file)
    timeout=$(grep -c "WORKER TIMEOUT" $file)
    keyerror=$(grep -c "KeyError: 'user'" $file)
    nonexistent=$(grep -c "does not appear to have an account" $file)
    # echo "| Username | $ERROR | $WARNING | $CRITICAL | $resultsp | $timeout | $keyerror | $nonexistent | $wc | $file |"
    echo "$username,$ERROR,$WARNING,$CRITICAL,$resultsp,$timeout,$keyerror,$nonexistent,$wc,$file"
done
