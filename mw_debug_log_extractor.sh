#!/bin/bash

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
# http://www.gnu.org/copyleft/gpl.html


# This script pulls statistics from the MediaWiki debug logs for each username
# looked up in Special:SimilarEditors.

# Assuming the mw debug logs are saved to a file ($mw_debug_log_file), it will:
# 1. split that file into separate files, one for each username looked up
# 2. derive statistics from each separate file, for example the errors reported
# 3. print a csv (or table) with a row for each username and columns for
#    different statistics

mw_debug_log_file=""
prefix="mwxx"
# You probably only want to run this line once, so comment out if you are
# running this script a second time
csplit -f $prefix $mw_debug_log_file "/\[http\] GET: http:\/\/[^:]*:5000\/similarusers?usertext=/" {*}

# echo "| Username | ERROR_count | WARNING_count | timeout | line_count | file_name |"
echo "username,ERROR_count,WARNING_count,timeout,file_name"
for file in $prefix*
do
    username=$(grep -o -P "(?<=usertext=).*" $file)
    ERROR=$(grep -m1 -i "ERROR\|ERROR" $file)
    WARNING=$(grep -m1 -i "WARNING\|WARNING" $file)
    timeout=$(grep -m1 -i "TIMEOUT\|TIMEOUT\|timed out" $file)
    # echo "| $username | $ERROR | $WARNING | $timeout | $wc | $file |"
    echo "$username,$ERROR,$WARNING,$timeout,$file"
done
