#!/usr/bin/python3

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
# http://www.gnu.org/copyleft/gpl.html


# This script will extract all the usernames that the similar-users service
# knows about.

# Run it from the `similar_users/resources/` directory. It will extract
# usernames from the coedit_counts.tsv, metadata.tsv and temporal.tsv files and
# return them as one list.

# E.g.
# 1. cd similar_users/resources/
# 2. python3 extract_users.py

import csv
import ipaddress

def isNotIP(name):
    try:
        ipaddress.ip_address(name)
        return False
    except:
        return True

users = []

with open('coedit_counts.tsv', newline='') as tsvfile:
    reader = csv.DictReader(tsvfile, delimiter="\t")
    for row in reader:
        if isNotIP(row['user_text']):
            users.append(row['user_text'])
        if isNotIP(row['user_neighbor']):
            users.append(row['user_neighbor'])

with open('metadata.tsv', newline='') as tsvfile:
    reader = csv.DictReader(tsvfile, delimiter="\t")
    for row in reader:
        if isNotIP(row['user_text']):
            users.append(row['user_text'])

with open('temporal.tsv', newline='') as tsvfile:
    reader = csv.DictReader(tsvfile, delimiter="\t")
    for row in reader:
        if isNotIP(row['user_text']):
            users.append(row['user_text'])

print(set(users))
