#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
TODO

The following parameters are supported:
-u, --username: Username or IP you want to investigate.

TODO
"""
#
# (C) Pywikibot team, 2004-2019
#
# Distributed under the terms of the MIT license.
#
import argparse

from bs4 import BeautifulSoup

import pywikibot
from pywikibot.comms.http import request


class SimilarEditors(object):

    """TODO"""

    def __init__(self, *args):
        """Initializer."""
        self.set_options(*args)
        self.site = pywikibot.Site()

    def set_options(self, *args):
        """Parse commandline and set options attribute."""
        my_args = pywikibot.handle_args(args)
        parser = argparse.ArgumentParser()
        parser.add_argument('-u', '--username', required=True)
        self.options = parser.parse_args(my_args)

    def run(self):
        """Run the bot."""
        self.site.login()
        params = {'wpTarget': self.options.username}
        r = request(site=self.site, uri="{}/Special:SimilarEditors".format(self.site.path()),
                    method="GET", params=params).text
        soup = BeautifulSoup(r, features="lxml")
        table = soup.find("table", class_="mw-datatable")
        error_msg = ""
        if table is not None:
            rows = table.find_all("tr")
        else:
            rows = []
            alert = soup.find("div", {"role": "alert"})
            if alert is not None:
                error_msg = alert.find("span", class_="oo-ui-labelElement-label").get_text()
        return '{},"{}"'.format(len(rows), error_msg)
        # self.site.logout()


def main(*args):
    """
    Process command line arguments and invoke bot.

    If args is an empty list, sys.argv is used.

    @param args: command line arguments
    @type args: str
    """
    app = SimilarEditors(*args)
    print(app.run())


if __name__ == '__main__':
    main()
